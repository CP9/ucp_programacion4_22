﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System;

using ARCHIVO;

namespace Estructura
{
    public class PantallaGanador : Pantalla
    {
        public static string Ganador;
        public static string Resultado;

        SpriteTexto ganador = new SpriteTexto();
        SpriteTexto resultado = new SpriteTexto();

        DateTime tiempo;

        public override void CargarContenido()
        {
            finalizada = false;
            tiempo = DateTime.Now;

            ganador.Color = Color.White;
            ganador.Ubicacion = new Vector2(10, 10);
            ganador.Tamaño = new Vector2(10, 10);
            ganador.Texto = Ganador + " Gana la partida"; 
            ganador.CargarContenido(this.content);


            resultado.Color = Color.White;
            resultado.Ubicacion = new Vector2(50, 200);
            resultado.Tamaño = new Vector2(10, 10);
            resultado.Texto =Resultado;
            resultado.CargarContenido(this.content);

            Archivo archivo = new Archivo();

            archivo.Carpeta = "Historial";
            archivo.VerificarCarpeta();

            string record = archivo.Leer("Highscore.txt");

            record += Ganador + "\t" + Resultado + "\r\n";

            archivo.Grabar("Highscore.txt", record);

        }

        public override void Draw(SpriteBatch sb)
        {
            resultado.Draw(sb);
            ganador.Draw(sb);
        }

        public override void Update()
        {
            if ((DateTime.Now - tiempo).TotalSeconds > 3)
            {
                finalizada = true;
            }
        }
    }
}