﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;


namespace Estructura
{
    public class Jugador : SpriteGraficoSolido
    {

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }




        public Rectangle Collider
        {
            get { 
                return new Rectangle((int)this.ubicacion.X, (int)this.ubicacion.Y, (int)this.tamaño.X, (int)this.tamaño.Y);
            }
       
        }



        private Keys arriba;

        public Keys Arriba
        {
            get { return arriba; }
            set { arriba = value; }
        }

        private Keys abajo;

        public Keys Abajo
        {
            get { return abajo; }
            set { abajo = value; }
        }

        
        public override void Update()
        {
            int movimiento = 5;
            if (Keyboard.GetState().IsKeyDown(arriba))
            {
                Mover(movimiento * -1);
            }
            else if (Keyboard.GetState().IsKeyDown(abajo))
            {
                Mover(movimiento );
            }

        }

        void Mover(int movimiento)
        {
            float y = this.ubicacion.Y + movimiento;

            if(y >= 150 &&  y + tamaño.Y <= 550 )
            { 
                this.ubicacion = new Vector2(this.ubicacion.X, y);
            }
        }




    }
}