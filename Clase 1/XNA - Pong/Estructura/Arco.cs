﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estructura
{
    public class Arco : SpriteGraficoSolido
    {

        private Jugador jugador;

        public Jugador Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }


        private Rectangle collider;

        public Rectangle Collider
        {
            get { return collider; }
        }
        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
            collider  = new Rectangle((int)this.ubicacion.X, (int)this.ubicacion.Y, (int)this.tamaño.X, (int)this.tamaño.Y);
        }
    }
}