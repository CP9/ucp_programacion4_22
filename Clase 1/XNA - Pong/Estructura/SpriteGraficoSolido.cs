﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estructura
{
    public class SpriteGraficoSolido : SpriteGrafico
    {
        private GraphicsDevice dispositivo;

        public GraphicsDevice Dispositivo
        {
            get { return dispositivo; }
            set { dispositivo = value; }
        }




        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);

            textura = new Texture2D(dispositivo, 1, 1, false, SurfaceFormat.Color);
            textura.SetData<Color>(new Color[] { color });


        }

        public override void Update()
        {
            
        }
        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);
        }
    }
}