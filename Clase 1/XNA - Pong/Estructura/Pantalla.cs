﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
namespace Estructura
{
    public abstract class Pantalla
    {

        //Qué número de pantalla soy?
        private int indice;

        public int Indice
        {
            get { return indice; }
            set { indice = value; }
        }

        private int siguienteIndice;

        public int SiguienteIndice
        {
            get { return siguienteIndice; }
            set { siguienteIndice = value; }
        }

        protected bool finalizada = false;

        public bool Finalizada
        {
            get {
                return finalizada;
            }
        }

        private GraphicsDevice graphicsDevice;

        public GraphicsDevice GraphicsDevice
        {
            get { return graphicsDevice; }
            set { graphicsDevice = value; }
        }



        protected ContentManager content;

        public ContentManager Content
        {
            get { return content; }
            set { content = value; }
        }


        public abstract void CargarContenido();

        public abstract void Update();   

        public abstract void Draw(SpriteBatch sb);




    }
}