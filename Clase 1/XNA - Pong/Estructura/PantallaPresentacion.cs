﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estructura
{
    public class PantallaPresentacion : Pantalla
    {

        SpriteTexto Titulo;


        SpriteTexto subtitulo;

        public override void CargarContenido()
        {
            finalizada = false;
            Titulo = new SpriteTexto( );
            Titulo.CargarContenido(this.content);
            Titulo.Color = Color.White;
            Titulo.Tamaño = new Vector2(20, 10);
            Titulo.Ubicacion = new Vector2(20, 20);
            Titulo.Texto = "Mi primer juego";


            subtitulo = new SpriteTexto();
            subtitulo.CargarContenido(this.content);
            subtitulo.Color = Color.White;
            subtitulo.Tamaño = new Vector2(20, 10);
            subtitulo.Ubicacion = new Vector2(50, 320);
            subtitulo.Texto = "presione start / enter para comenzar";



        }

        public override void Draw(SpriteBatch sb)
        {

            Titulo.Draw(sb);
            subtitulo.Draw(sb);
        }

        public override void Update()
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                SiguienteIndice = -1;
                finalizada = true;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {                
                finalizada = true;
            }
        }
    }
}