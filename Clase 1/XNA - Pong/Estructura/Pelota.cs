﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Estructura
{
    public class Pelota : SpriteGraficoSolido
    {

        private Vector2 ubicacionInicial;

        public Vector2 UbicacionInicial
        {
            get { return ubicacionInicial; }
            set { ubicacionInicial = value; }
        }

        


        private float velx =-6f;

        public float VelX
        {
            get { return velx; }
            set { velx = value; }
        }

        private float vely = -4;

        public float VelY
        {
            get { return velx; }
            set { velx = value; }
        }


        public override void Update()
        {

            float Y = ubicacion.Y + vely;

            float X = ubicacion.X + velx;

            if (Y <= 150 || Y + tamaño.Y >= 550)
            {
                vely *= -1;

            }
            else
            {
                ubicacion = new Vector2(X, Y);
            
            }

        }

        private Rectangle collider;

        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
            ubicacionInicial = ubicacion;
            
        }


        public void VerificarColision(Jugador jugador)
        {
            collider = new Rectangle((int)this.ubicacion.X, (int)this.ubicacion.Y, (int)this.tamaño.X, (int)this.tamaño.Y);
            if (jugador.Collider.Intersects(this.collider))
            {
                velx *= -1;
            }


        }

        public void VerificarColision(Arco arco)
        {
            collider = new Rectangle((int)this.ubicacion.X, (int)this.ubicacion.Y, (int)this.tamaño.X, (int)this.tamaño.Y);
            if (arco.Collider.Intersects(this.collider))
            {
                velx *= -1;

                arco.Jugador.Puntos += 1;

                ubicacion = UbicacionInicial;
            }


        }


    }
}