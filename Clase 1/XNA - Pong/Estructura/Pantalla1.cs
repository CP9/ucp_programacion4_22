﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Estructura
{
    public class Pantalla1 : Pantalla
    {

        SpriteGraficoSolido cancha = new SpriteGraficoSolido();

        Jugador p1;

        Jugador p2 ;

        SpriteTexto puntaje = new SpriteTexto();

        Pelota pelota = new Pelota();

        Arco arco1 = new  Arco();
        Arco arco2 = new Arco();

        public override void CargarContenido()
        {
            finalizada = false;

             p2 = new Jugador();
             p1 = new Jugador();



            cancha.Dispositivo = GraphicsDevice;
            cancha.Color = Color.Green;
            cancha.Tamaño = new Vector2(800, 400);
            cancha.Ubicacion =new  Vector2(0, 150);
            
            cancha.CargarContenido(this.content);

            p1.Dispositivo = GraphicsDevice;
            p1.Color = Color.White;
            p1.Tamaño = new Vector2(20, 50);

                                   //(x, offset - mitad de cancha - mitad de la paleta)    
            p1.Ubicacion = new Vector2(10, 150 + 200 - 25);
            p1.Abajo = Keys.Down;
            p1.Arriba = Keys.Up;
            p1.CargarContenido(this.content);


            p2.Dispositivo = GraphicsDevice;
            p2.Color = Color.White;
            p2.Tamaño = new Vector2(20, 50);
            //(x, offset - mitad de cancha - mitad de la paleta)    
            p2.Ubicacion = new Vector2(770, 150 + 200 - 25);
            p2.Abajo = Keys.S;
            p2.Arriba = Keys.W;
            p2.CargarContenido(this.content);


            puntaje.Color = Color.Red;
            puntaje.Texto = "";
            puntaje.Ubicacion = new Vector2(380, 20);
            puntaje.CargarContenido(this.content);

            pelota.Dispositivo = GraphicsDevice;

            pelota.Color = Color.Violet;
            pelota.Tamaño = new Vector2(20, 20);
            pelota.Ubicacion = new Vector2(390,265 );
            pelota.CargarContenido(this.content);

            CrearArco(-40,150, arco1, p2 );
            CrearArco(790, 150, arco2,p1);

        }

        private void CrearArco(int x, int y, Arco arco, Jugador j)
        {
            arco.Dispositivo = GraphicsDevice;
            arco.Color = Color.Chocolate;
            arco.Tamaño = new Vector2(50, 400);
            arco.Ubicacion = new Vector2(x, y);
            arco.CargarContenido(this.content);
            arco.Jugador = j;
        }




        public override void Draw(SpriteBatch sb)
        {
            cancha.Draw(sb);
            arco1.Draw(sb);
            arco2.Draw(sb);
            p1.Draw(sb);
            p2.Draw(sb);
            pelota.Draw(sb);
            puntaje.Draw(sb);
          

        }

        public override void Update()
        {
            pelota.VerificarColision(p1);
            pelota.VerificarColision(p2);

            pelota.VerificarColision(arco1);
            pelota.VerificarColision(arco2);

            p1.Update();
            p2.Update();
            pelota.Update();

            puntaje.Texto = p1.Puntos.ToString() + " | " + p2.Puntos.ToString();

            finalizada = (p1.Puntos == 3 || p2.Puntos == 3);

            if (finalizada)
            {
                if (p1.Puntos == 3)
                {
                    PantallaGanador.Ganador = "Jugador 1";
                }
                else
                {
                    PantallaGanador.Ganador = "Jugador 2";
                }
                PantallaGanador.Resultado = puntaje.Texto;

            }

        }
    }
}