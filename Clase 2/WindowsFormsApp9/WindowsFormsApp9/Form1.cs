﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Datos;
namespace WindowsFormsApp9
{
    public partial class Form1 : Form
    {
        Acceso acceso = new Acceso();
        public Form1()
        {
            InitializeComponent(); 
            acceso.Conectar();
        }

        ~Form1()
        {
            acceso.Desconectar();
            acceso = null;
            GC.Collect();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "select isnull(max(id), 0) +1 from Puntuacion";

            int id = acceso.DevolverEscalar(sql);
            
            label1.Text = id.ToString();

            sql = "Insert into Puntuacion (id,nombre,highscore,fecha) ";
            sql += "values (" + id.ToString() + ",'" + textBox1.Text + "'," + textBox2.Text + ", getdate()  )";

            acceso.Escribir(sql);
            Enlazar();
        }



        public void Enlazar()
        {
            SqlDataReader lector = acceso.Leer("select * from Puntuacion order by highscore desc");
            List<Puntuacion> puntuaciones = new List<Puntuacion>();
            while (lector.Read())
            {
                Puntuacion p = new Puntuacion();
                p.ID = int.Parse(lector["id"].ToString());
                p.Nombre = lector["nombre"].ToString();
                p.Highscore = lector.GetInt32(2);
                p.Fecha = lector["fecha"].ToString();

                puntuaciones.Add(p);
            }

            lector.Close();
            lector = null;

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = puntuaciones;
            GC.Collect();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Puntuacion p;

            p = (Puntuacion)dataGridView1.Rows[e.RowIndex].DataBoundItem;

            textBox3.Text = p.ID.ToString();
            textBox2.Text = p.Highscore.ToString();
            textBox1.Text = p.Nombre;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = "update Puntuacion set Nombre = '" + textBox1.Text + "', Highscore = " + textBox2.Text  + " where id = " + textBox3.Text;
            acceso.Escribir(sql);
            Enlazar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sql = "Delete from puntuacion where id=" + textBox3.Text;
            acceso.Escribir(sql);
            Enlazar();
        }
    }
}
