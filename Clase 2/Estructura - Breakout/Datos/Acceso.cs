﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Datos
{
    public class Acceso
    {
        // 1  -> Connection
        // 2  -> Command  
        // 3  -> DataReader

        private SqlConnection conexion;

        public void Conectar()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = @"Initial Catalog=ProgIV; Data Source=.;Integrated Security=SSPI ";
            conexion.Open();
        }

        public void Desconectar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string sql)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.Text;
            comando.CommandText = sql;
            return comando;
        }

        public int DevolverEscalar(string sql)
        {
            SqlCommand comando = CrearComando(sql);

            int resultado = int.Parse( comando.ExecuteScalar().ToString());

            return resultado;

        }

        public void Escribir(string sql)
        {
            SqlCommand comando = CrearComando(sql);

            comando.ExecuteNonQuery();
        }


        public SqlDataReader Leer(string sql)
        {
            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            return lector;
        }



    }
}
