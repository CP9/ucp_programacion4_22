﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Estructura
{
    public class PantallaPresentacion : Pantalla
    {
        SpriteTexto titulo;

        SpriteTexto record;

        SpriteTexto FPS = new SpriteTexto();
        public override void CargarContenido()
        {
            titulo = new SpriteTexto();
            titulo.CargarContenido(Content);
            titulo.Color = Color.White;
            titulo.Texto = "Breakout";
            titulo.Ubicacion = new Vector2(300, 300);
            titulo.Tamaño = new Vector2(10, 10);

            FPS.CargarContenido(Content);
            FPS.Color = Color.Red;
            FPS.Texto = "";
            FPS.Ubicacion = new Vector2(550, 25);
            FPS.Tamaño = new Vector2(400, 25);

            record = new SpriteTexto();
            record.CargarContenido(Content);
            record.Color = Color.White;
            record.Texto = "Highscore: " + Jugador.Highscore.ToString() ;
            record.Ubicacion = new Vector2(300, 10);
            record.Tamaño = new Vector2(10, 10);
            finalizada = false;
        }

        public override void Draw(SpriteBatch sb)
        {
            titulo.Draw(sb);
            record.Draw(sb);

            FPS.Draw(sb);
        }

        private DateTime ultimoframe = DateTime.Now;
        private int contadorFrames;
        public override void Update()
        {

        #if DEBUG
            if ((DateTime.Now - ultimoframe).TotalSeconds > 1)
            {
                ultimoframe = DateTime.Now;
                FPS.Texto = contadorFrames.ToString();
                contadorFrames = 0;
            }
            else
            {
                contadorFrames++;
            }
        #endif



            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                this.finalizada = true;
            }
        }
    }
}