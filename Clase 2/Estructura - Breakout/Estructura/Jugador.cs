﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Estructura
{
    class Jugador:SpriteGraficoSolido, ICollider
    {
        private int puntos = 0;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        private static int highscore;

        public static int Highscore
        {
            get { return highscore; }
            set { highscore = value; }
        }



        private int vidas;

        public int Vidas
        {
            get { return vidas; }
            set { vidas = value; }
        }

        private Collider collider;

        public Collider Collider
        {
            get { return collider; }
            
        }

        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
            collider = new Collider();
            collider.Sprite = this;
        }

        int velocidad = 10;


        public override void Update()
        {
            collider.Update();

            KeyboardState kb = Keyboard.GetState();
            float x =this.ubicacion.X;

            if (kb.IsKeyDown(Keys.Left))
            {
                x = x - velocidad;
                if (x < 0)
                {
                    x = 0;
                }
            }
            else if (kb.IsKeyDown(Keys.Right))
            {
                x = x + velocidad;
                if (x + tamaño.X > 600)
                {
                    x = 500;
                }
            }

            ubicacion = new Vector2(x, ubicacion.Y);
           


        }

        public bool VerificarColision(ICollider sp)
        {
            bool pega = collider.VerificarColision(sp);

            if (pega)
            {
                ((Pelota)sp).angulo += 90;
            }

            return pega;
        }

        public Collider DevolverCollider()
        {
            return collider;
        }
    }
}
