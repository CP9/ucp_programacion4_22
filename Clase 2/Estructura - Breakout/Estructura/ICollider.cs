﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Estructura
{
    public interface ICollider
    {
        bool VerificarColision(ICollider sp);

        Collider DevolverCollider();
    }
}