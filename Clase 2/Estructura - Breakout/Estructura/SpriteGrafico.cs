﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estructura
{
    public abstract class SpriteGrafico : Sprite
    {
        protected Texture2D textura;


        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
        }


        public override void Draw(SpriteBatch sb)
        {
            Rectangle rect = new Rectangle((int) ubicacion.X, (int) ubicacion.Y, (int) tamaño.X,(int)tamaño.Y  );


            sb.Draw(textura, rect, color);
        }
       
    }
}