﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Estructura
{
    public class Collider
    {
        private Sprite sprite;

        private Rectangle rectangle;

        public Sprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }

        public void Update()
        {
            rectangle = new Rectangle((int)sprite.Ubicacion.X, (int)sprite.Ubicacion.Y, (int)sprite.Tamaño.X, (int)sprite.Tamaño.Y);        
        }

        public bool VerificarColision(ICollider otro)
        {
            return rectangle.Contains(otro.DevolverCollider().rectangle);
        }

    }
}