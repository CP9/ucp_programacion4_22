﻿using System;

namespace Estructura
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Principal())
                game.Run();
        }
    }
}
