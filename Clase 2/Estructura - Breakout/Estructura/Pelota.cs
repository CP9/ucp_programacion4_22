﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Estructura
{
    class Pelota:SpriteGraficoSolido,ICollider
    {
        float velX = 10;
        float velY = 10;

        public float angulo = 75;

        private bool piso =false;

        public bool Piso
        {
            get { return piso; }
            
        }
        private Collider collider;

        public Collider Collider
        {
            get { return collider; }
            
        }

        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
            collider = new Collider();
            collider.Sprite = this;
        }
        public override void Update()
        {

            float x = ubicacion.X + velX * (float) Math.Cos(angulo)  ;
            float y = ubicacion.Y + velY * (float)Math.Sin(angulo);

            if (x >= 580 || x <=0)
            {
                velX *= -1;             
            }
            if (y <= 0)
            {
                velY *= -1;
            }
            if (y >= 580 )
            {
                piso = true;                
            }
            ubicacion = new Vector2(x, y);

            collider.Update();


        }

        public bool VerificarColision(ICollider sp)
        {
            throw new NotImplementedException();
        }

        public Collider DevolverCollider()
        {
            return collider;
        }
    }
}
