﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace Estructura
{
    public class PantallaRecords : Pantalla
    {
        DataTable tabla = new DataTable();
        List<SpriteTexto> records = new List<SpriteTexto>();

   
        public override void CargarContenido()
        {
            SQLiteConnection conexion = new SQLiteConnection();
            conexion.ConnectionString = @"Data Source=.\Puntuacion.db; Version=3";
            conexion.Open();
            SQLiteDataAdapter adaptador = new SQLiteDataAdapter();
            adaptador.SelectCommand = new SQLiteCommand();
            adaptador.SelectCommand.CommandText = "Select * from puntuacion order by puntos desc limit 10";
            adaptador.SelectCommand.CommandType = CommandType.Text;
            adaptador.SelectCommand.Connection = conexion;
            adaptador.Fill(tabla);
            conexion.Close();
            conexion = null;

            int indice = 1;
            foreach (DataRow registro in tabla.Rows)
            {
                SpriteTexto texto = new SpriteTexto();
                texto.CargarContenido(Content);
                texto.Color = Color.White;
                texto.Texto = registro["Inicial"].ToString() + "     " + registro["Puntos"].ToString() ;
                texto.Ubicacion = new Vector2(300, 300 + 20 * indice);
                texto.Tamaño = new Vector2(10, 10);
                indice++;
                records.Add(texto);
            }
        }

        public override void Draw(SpriteBatch sb)
        {
            foreach (SpriteTexto texto in records)
            {
                texto.Draw(sb);
            }
        }

        public override void Update()
        {
            
        }
    }
}