﻿using Cocos2D;
using CocosDenshion;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Estructura
{
  
    public class Principal : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<Pantalla> pantallas = new List<Pantalla>();

        Pantalla pantallaActual = null;


        public Principal()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";         

        }

        protected override void Initialize()
        {
      

            base.Initialize();
        }

        protected override void LoadContent()
        {

            spriteBatch = new SpriteBatch(GraphicsDevice);
            //Crear pantallas

            IsMouseVisible = true;

            
            Pantalla p1 = new PantallaPresentacion();
            CrearPantalla(p1, 0, 1, true);
            
            p1 = new Pantalla1();
            CrearPantalla(p1, 1, 2, false);

            p1 = new PantallaRegistro();
            CrearPantalla(p1, 2, 3, false);

            p1 = new PantallaRecords();
            CrearPantalla(p1, 3, 0, false);


            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            graphics.ApplyChanges();

           


        }


        private void CrearPantalla(Pantalla p1, int indice, int indicesiguiente, bool cargarcontenido )
        {
            p1.Content = this.Content;
            p1.Indice = indice;
            p1.SiguienteIndice = indicesiguiente;

            p1.GraphicsDevice = this.GraphicsDevice;
            if (cargarcontenido)
            {
                p1.CargarContenido();
                pantallaActual = p1;
            }
            pantallas.Add(p1);
        }

        protected override void UnloadContent()
        {
           
        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.F12))
            {
                graphics.ToggleFullScreen();
            }
           
            pantallaActual.Update();

            if (pantallaActual.Finalizada)
            {
                if (pantallaActual.SiguienteIndice == -1)
                {
                    ExitGame();
                }
                else
                {
                   
                    pantallaActual = pantallas[pantallaActual.SiguienteIndice];                  
                    pantallaActual.CargarContenido();
                }
            }

      

            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            pantallaActual.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void ExitGame()
        {
            
            CCSimpleAudioEngine.SharedEngine.RestoreMediaState();
            Exit();
        }
    }
}
