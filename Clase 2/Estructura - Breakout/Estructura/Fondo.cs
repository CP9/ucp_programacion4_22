﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Estructura
{
    public class Fondo : SpriteGrafico
    {
        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
            textura = Content.Load<Texture2D>("Fondo");

        }

        public override void Update()
        {
            
        }
    }
}