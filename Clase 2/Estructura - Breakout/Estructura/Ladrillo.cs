﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estructura
{
    class Ladrillo:SpriteGraficoSolido,ICollider
    {
        private int resistencia  =1;
         
        private bool mostrar =true;

        public bool Mostrar
        {
            get { return mostrar; }
            
        }


        public int Resistencia
        {
            get { return resistencia; }
            set { resistencia = value; }
        }

        private Collider collider;

        public Collider Collider
        {
            get { return collider; }
            
        }

        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);
            collider = new Collider();
            collider.Sprite = this;
        }

        public override void Update()
        {
            base.Update();
            collider.Update();
        }

        public Collider DevolverCollider()
        {
            return collider;
        }

        public bool VerificarColision(ICollider sp)
        {
            bool pega = collider.VerificarColision(sp);
            if (pega)
            {
                ((Pelota)sp).angulo += 180;
                resistencia--;
                mostrar = resistencia > 0;
            }
            return pega;

        }
    }
}
