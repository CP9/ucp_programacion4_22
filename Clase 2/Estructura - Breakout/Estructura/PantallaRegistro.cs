﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace Estructura
{
    public class PantallaRegistro : Pantalla
    {
        SpriteTexto escritura = new SpriteTexto();
        List<SpriteTexto> lista = new List<SpriteTexto>();
        SpriteGraficoSolido fondo = new SpriteGraficoSolido();
        List<string> letras = new List<string>();

        int indice = 0;
        public override void CargarContenido()
        {
            
            for (int i = 65; i<91; i++ )
            {
                SpriteTexto texto = new SpriteTexto();
                texto.CargarContenido(Content);
                texto.Color = Color.White;
                texto.Texto = ((char)i).ToString() ;
                texto.Ubicacion = new Vector2(200 +20 * indice , 300 );
                texto.Tamaño = new Vector2(10, 10);

                letras.Add(((char)i).ToString());
                indice++;
                lista.Add(texto);

            }

         
            escritura.CargarContenido(Content);
            escritura.Color = Color.White;
            escritura.Texto = "";
            escritura.Ubicacion = new Vector2(10, 10);
            escritura.Tamaño = new Vector2(10, 10);
            indice++;


            fondo.Color = Color.Orange;
            fondo.Tamaño = new Vector2(20, 20);
            fondo.Ubicacion = new Vector2(0, 0);
            fondo.Dispositivo = GraphicsDevice;            
            fondo.CargarContenido(Content);

        }

        public override void Draw(SpriteBatch sb)
        {
            if (mostrarrecuadro)
            {
                fondo.Draw(sb);
            }


            foreach (SpriteTexto texto in lista)
            {
                texto.Draw(sb);
            }
            escritura.Draw(sb);

         
        }

        public bool mostrarrecuadro = false;

        MouseState MSPrevio;
        public override void Update()
        {
            MouseState ms = Mouse.GetState();

            mostrarrecuadro = false;

            if (ms.Y >= 300 && ms.Y < 320)
            {
                if (ms.X >= 200 && ms.X < 720)
                {
                    int indice = (int)((ms.X - 200) / 20);

                    int res = 200 + indice* 20;
                    fondo.Ubicacion = new Vector2( res, 305   );
                    mostrarrecuadro = true;

                    if (ms.LeftButton== ButtonState.Pressed ) 
                    {
                        if(MSPrevio == null || MSPrevio.LeftButton == ButtonState.Released)
                        { 
                            escritura.Texto += letras[indice];
                        }
                    }

                    MSPrevio = ms;

                }
            }

            if (escritura.Texto.Length == 3)
            {
                finalizada = true;

                SQLiteConnection conexion = new SQLiteConnection();
                conexion.ConnectionString = @"Data Source=.\Puntuacion.db; Version=3";
                conexion.Open();

                SQLiteCommand comando = new SQLiteCommand();
                comando.CommandText = "insert into Puntuacion (INICIAL, PUNTOS) values (@I, @P)";
                comando.CommandType = CommandType.Text;
                comando.Connection = conexion;

                comando.Parameters.AddWithValue("@I", escritura.Texto);
                comando.Parameters.AddWithValue("@P",Jugador.Highscore );

                comando.ExecuteNonQuery();

                conexion.Close();
                conexion = null;


            }
   


        }
    }
}