﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Estructura
{
    public abstract class Sprite
    {


        protected Color color;

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        protected Vector2 ubicacion;

        public Vector2 Ubicacion
        {
            get { return ubicacion; }
            set { ubicacion = value; }
        }

        protected Vector2 tamaño;

        public Vector2 Tamaño
        {
            get { return tamaño; }
            set { tamaño = value; }
        }

        protected ContentManager Content;

        public virtual void CargarContenido( ContentManager cm )
        {
            this.Content = cm;
        }


        public abstract void Update();

        public abstract void Draw(SpriteBatch sb);
    }
}