﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using System.Linq;


namespace Estructura
{
    class Pantalla1 : Pantalla
    {

        Fondo fondo = new Fondo();
        Jugador jugador = new Jugador();
        Pelota pelota;

        List<Ladrillo> Ladrillos = new List<Ladrillo>();

        public override void CargarContenido()
        {
            fondo.Color = Color.White;
            fondo.Tamaño = new Vector2(600, 600);
            fondo.Ubicacion = new Vector2(0, 0);            
            fondo.CargarContenido(Content);

            jugador.Color = Color.White;
            jugador.Tamaño = new Vector2(100, 20);
            jugador.Ubicacion = new Vector2(250, 570);
            jugador.Dispositivo = GraphicsDevice;
            jugador.Vidas = 3;
            jugador.CargarContenido(Content);

            for (int fila = 0; fila < 5; fila++)
            {
                for(int columna = 0; columna <12; columna++  )
                { 
                    Ladrillo ladrillo = new Ladrillo();
                    if (fila == 0)
                    {
                        ladrillo.Resistencia = 2;
                        ladrillo.Color = Color.Yellow;
                    }
                    else
                    {
                        ladrillo.Color = Color.Red;
                    }
                    ladrillo.Ubicacion =new Vector2(columna * 50, 20 + (20*fila) );
                    ladrillo.Tamaño = new Vector2(50, 20);
                    ladrillo.Dispositivo = GraphicsDevice;
                    ladrillo.CargarContenido(Content);
                    Ladrillos.Add(ladrillo);
                }
            }
            CrearPelota();
            finalizada = false;
        }

        private void CrearPelota()
        {
            pelota = new Pelota();
            pelota.Color = Color.Green;
            pelota.Tamaño = new Vector2(10, 10);
            pelota.Ubicacion = new Vector2(250, 570);
            pelota.Dispositivo = GraphicsDevice;
            pelota.CargarContenido(Content);
        }

        public override void Draw(SpriteBatch sb)
        {
            fondo.Draw(sb);

            jugador.Draw(sb);

            foreach (Ladrillo ladrillo in Ladrillos)
            {
                ladrillo.Draw(sb);
            }

            pelota.Draw(sb);


        }

        public override void Update()
        {
            jugador.Update();
            pelota.Update();

            if (!jugador.VerificarColision(pelota))
            {
                int indice = 0;
                bool pega = false;
                do
                {
                    Ladrillos[indice].Update();
                    pega = Ladrillos[indice].VerificarColision(pelota);
                    if (pega)
                    {
                        jugador.Puntos += 10;
                    }

                    indice++;
                } while (indice < Ladrillos.Count );

            }

            List<Ladrillo> LadrillosEliminar = (from Ladrillo Lad in Ladrillos
                                                where !Lad.Mostrar
                                                select Lad
                                                ).ToList(); ;

            foreach (Ladrillo lad in LadrillosEliminar)
            {
                Ladrillos.Remove(lad);
            }

            LadrillosEliminar.Clear();

            if (pelota.Piso)
            {
                jugador.Vidas--;
                pelota = null;
                CrearPelota();
            }
            this.finalizada = (jugador.Vidas <= 0);
            if (finalizada && jugador.Puntos > Jugador.Highscore)
            {
                Jugador.Highscore = jugador.Puntos;
                this.SiguienteIndice = 2;
            }
            else
            {
                this.SiguienteIndice = 3;
            }

                        
        }
    }
}
