﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace Estructura
{
    public class SpriteTexto : Sprite
    {

        private SpriteFont fuente;

        private string texto;

        private bool update = false;

        public string Texto
        {
            get { return texto; }
            set { texto = value; }
        }


        private string archivofuente = @"fonts\MarkerFelt-16";

        public string ArchivoFuente
        {
            get { return archivofuente; }
            set { archivofuente = value;
                update = true;
                }
        }




        public override void CargarContenido(ContentManager cm)
        {
            base.CargarContenido(cm);

            fuente = Content.Load<SpriteFont>(archivofuente);
        }

        public override void Draw(SpriteBatch sb)
        {
            if(!string.IsNullOrWhiteSpace(texto))
            { 
                sb.DrawString(fuente, texto, this.ubicacion, color);
            }

        }

        public override void Update()
        {
            if (update)
            {
                fuente = Content.Load<SpriteFont>(archivofuente);
                update = false;
            }

        }
    }
}