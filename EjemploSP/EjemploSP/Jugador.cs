﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace EjemploSP
{
    class Jugador
    {
        private Acceso acceso = new Acceso();
        private  int id ;

        public int Id
        {
            get { return id; }
            set { id= value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }


        public void Grabar()
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@jug", this.nombre));
            parametros.Add(acceso.CrearParametro("@puntos", this.puntos));
            string sp;
            if (id == 0)
            {
                sp = "Player_Insertar";
            }
            else
            {
                sp = "jugador_editar";
                parametros.Add(acceso.CrearParametro("@id", this.id));
            }
            acceso.Abrir();
            acceso.Escribir(sp, parametros);
            acceso.Cerrar();
        }

        public static List<Jugador> Leer()
        {
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Jugador_Listar");

            acceso.Cerrar();

            List<Jugador> jugadores = new List<Jugador>();

            foreach (DataRow registro in tabla.Rows)
            {
                Jugador j = new Jugador();
                j.id = int.Parse(registro["ID"].ToString());
                j.nombre = registro["Jugador"].ToString();
                j.puntos = int.Parse(registro["puntos"].ToString());
                jugadores.Add(j);
            }

            return jugadores;
        }
    }
}
