﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploSP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Jugador jugador = new Jugador();
            jugador.Nombre = textBox1.Text;
            jugador.Puntos = int.Parse(textBox2.Text);

            jugador.Grabar();

            Enlazar();

        }

        void Enlazar()
        {
            grilla.DataSource = null;
            grilla.DataSource = Jugador.Leer();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void grilla_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                Jugador j = (Jugador)grilla.Rows[e.RowIndex].DataBoundItem;

                label1.Text = j.Id.ToString();
                textBox1.Text = j.Nombre;
                textBox2.Text = j.Puntos.ToString();
            }
            catch
            { }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Jugador jugador = new Jugador();
            jugador.Id = int.Parse(label1.Text);
            jugador.Nombre = textBox1.Text;
            jugador.Puntos = int.Parse(textBox2.Text);

            jugador.Grabar();

            Enlazar();
        }
    }
}
