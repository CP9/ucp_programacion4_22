﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace EjemploSP
{
    class Acceso
    {
        private SqlConnection conexion;
        public void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=.; Initial Catalog=progiv_3; integrated Security=SSPI";
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        public SqlCommand CrearComando(string nombreSP, List<SqlParameter> parametros = null)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = nombreSP;

            if (parametros != null)
            {
                cmd.Parameters.AddRange(parametros.ToArray());
            }

            return cmd;

        }


        public DataTable Leer(string nombreSP, List<SqlParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(nombreSP, parametros);
            DataTable tabla = new DataTable();

            adaptador.Fill(tabla);
            adaptador = null;
            return tabla;
        
        }


        public int Escribir(string nombreSP, List<SqlParameter> parametros = null)
        {
            SqlCommand cmd = CrearComando(nombreSP, parametros);
            int res = 0;
            try
            {
                res = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                res = -1;
            }

            return res;

        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter();
            p.Value = valor;
            p.ParameterName = nombre;
            p.DbType = DbType.String;
            return p;
        }
        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter();
            p.Value = valor;
            p.ParameterName = nombre;
            p.DbType = DbType.Int32;
            return p;
        }

    }
    }
