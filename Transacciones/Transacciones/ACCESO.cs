﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Transacciones
{
    class ACCESO
    {
        private SqlConnection conexion;
        private SqlTransaction tx;



        public void Abrir()
        {
            conexion = new SqlConnection("Initial Catalog=PROG_IV_2;Data Source=.; Integrated Security=SSPI");
            conexion.Open();
        }
        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        public void IniciarTx()
        {
            tx = conexion.BeginTransaction();
        }


        public void ConfimarTX()
        {
            tx.Commit();
            tx = null;
        }
        public void CancelarTX()
        {
            tx.Rollback();
            tx = null;
        }

        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null)
        {
            SqlCommand cmd = new SqlCommand(sql, conexion);
            if (parametros != null)
            {
                cmd.Parameters.AddRange(parametros.ToArray());
            }

            if (tx != null)
            {
                cmd.Transaction = tx;
            }

            cmd.CommandType = CommandType.Text;
            return cmd;
        }

        public DataTable Leer(string sql, List<SqlParameter> parametros = null)
        {
            DataTable tabla = new DataTable();
            using (SqlDataAdapter da = new SqlDataAdapter())
            {
                da.SelectCommand = CrearComando(sql, parametros);
                da.Fill(tabla);
                da.Dispose();
            }
            return tabla;

        }
        public int Escibir(string sql, List<SqlParameter> parametros = null)
        {
            SqlCommand cmd = CrearComando(sql, parametros);
            int res = 0;
            try
            {
                res = cmd.ExecuteNonQuery();
            }
            catch
            {
                res = -1;
            }
            return res;
        }
    }
}
