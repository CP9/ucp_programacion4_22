﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Transacciones
{
    public partial class Form1 : Form
    {
        ACCESO ac = new ACCESO();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Enlazar();
        }
        void Enlazar()
        {
          
            grilla.DataSource = null;
            grilla.DataSource = ac.Leer("Select * from usuario");
        
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         
            ac.Abrir();
            Enlazar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ac.IniciarTx();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int i = 11; i < 1011; i++)
            {
                string sql = "insert into usuario values(" + i.ToString() + ",'aaa',1,1,55)";
                ac.Escibir(sql);

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ac.ConfimarTX();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ac.CancelarTX();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ac.Cerrar();
        }
    }
}
